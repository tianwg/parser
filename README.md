Java
----
***How to compile***

Run ```./gradlew customFatJar```
After run, the jar can be found at ```./build/libs```


***Java program that can be run from command line***

parser.jar file can be found at root.

```
java -cp "parser.jar" com.ef.Parser --accesslog=/path/to/file --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100
```



SQL
---
***MySQL schema used for the log data***

    ```
    CREATE TABLE ACCESS_LOG
    (
      DATE       TIMESTAMP     NULL,
      IP         VARCHAR(20)   NULL,
      REQUEST    VARCHAR(50)   NULL,
      STATUS     VARCHAR(3)    NULL,
      USER_AGENT VARCHAR(1000) NULL,
      COMMENT    VARCHAR(1000) NULL
    );
    ```


***Write MySQL query to find IPs that mode more than a certain number of requests for a given time period.***
```
SELECT IP, COUNT(*)
FROM ACCESS_LOG
WHERE DATE > TIMESTAMP('2017-01-01.13:00:00') AND DATE < TIMESTAMP('2017-01-01.14:00:00')
GROUP BY IP
HAVING COUNT(IP) > 20;
```


***Write MySQL query to find requests made by a given IP.***

```
SELECT IP, COUNT(*) FROM ACCESS_LOG GROUP BY IP;
```