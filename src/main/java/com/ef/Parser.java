package com.ef;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingByConcurrent;

public class Parser {

  public static void main(String[] args) throws FileNotFoundException, IllegalAccessException, ClassNotFoundException, InstantiationException {
    Map<String, String> params = getParam(args);
    String filePath = params.get("accesslog");
    String startDateString = params.get("startDate");
    LocalDateTime startDate =
        LocalDateTime.parse(startDateString, DateTimeFormatter.ofPattern("yyyy-MM-dd.HH:mm:ss"));
    String duration = params.get("duration");
    int threshold = Integer.parseInt(params.get("threshold"));
    Scanner scanner = new Scanner(new File(filePath));

    // read all logs to a list
    List<AccessLog> logs = new ArrayList<>();
    while (scanner.hasNextLine()) {
      logs.add(parseLog(scanner.nextLine()));
    }

    Map<String, Long> temp = new HashMap<>();
    Map<String, Long> results;

    // filter result based on startDate first.  check duration type, for different duration, select different time periods. using collectors counting() method to count the requests of one ip
    if ("hourly".equals(duration)) {
      temp =
          logs.stream()
              .parallel()
              .filter(
                  log ->
                      log.getDate().isAfter(startDate)
                          && log.getDate().isBefore(startDate.plusHours(1)))
              .collect(groupingByConcurrent(AccessLog::getIp, counting()));

    } else if ("daily".equals(duration)) {
      temp =
          logs.stream()
              .parallel()
              .filter(
                  log ->
                      log.getDate().isAfter(startDate)
                          && log.getDate().isBefore(startDate.plusDays(1)))
              .collect(groupingByConcurrent(AccessLog::getIp, counting()));
    }


    // filter the results based on threshold
    results =
        temp.entrySet()
            .stream()
            .filter(entry -> entry.getValue() >= threshold)
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    // Set the block reason to each log
    Map<String, Long> finalResults = results;
    logs.parallelStream()
        .forEach(
            log -> {
              if (finalResults.containsKey(log.getIp())) {
                log.setComment("Blocked due to request reach threshold " + threshold + " " + duration);
              }
            });

    int threadsCounter = 0;
    // make each thread process 5000 record. and create logs.size()/5000 threads for concurrency
    for (int i = 0; i < logs.size(); i+=2000) {
      threadsCounter++;
      LogRepo dbLogger = new LogRepo(logs.subList(i, Math.min(i+2000, logs.size())));
      Thread thread = new Thread(dbLogger);
      thread.setName("Log-repo-thread-" + threadsCounter);
      thread.start();
    }
    System.out.println(results.keySet());
  }

  private static AccessLog parseLog(String entry) {
    String[] logEntry = entry.split("\\|");
    String dateString = logEntry[0];
    LocalDateTime date =
        LocalDateTime.parse(dateString, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
    String ip = logEntry[1];
    String request = logEntry[2];
    String status = logEntry[3];
    String userAgent = logEntry[4];
    return new AccessLog(date, ip, request, status, userAgent, "");
  }

  private static Map<String, String> getParam(String[] args) {
    String paramPattern = "--(.+)=(.+)";
    Pattern pattern = Pattern.compile(paramPattern);
    return Arrays.stream(args)
        .filter(arg -> pattern.matcher(arg).find())
        .collect(
            Collectors.toMap(
                arg -> {
                  Matcher matcher = pattern.matcher(arg);
                  matcher.find();
                  return matcher.group(1);
                },
                arg -> {
                  Matcher matcher = pattern.matcher(arg);
                  matcher.find();
                  return matcher.group(2);
                }));
  }
}
