package com.ef;

import java.sql.*;
import java.util.List;
import java.util.Objects;

public class LogRepo implements Runnable {

  private List<AccessLog> accessLogs;

  // better to use thread pool for concurrent inserting
  private Connection connection = Objects.requireNonNull(getConnection());
  String insertQuery =
      "INSERT INTO ACCESS_LOG (DATE, IP, REQUEST, STATUS, USER_AGENT, COMMENT) VALUES (?, ? ,? , ?, ?, ?);";

  public LogRepo(List<AccessLog> accessLogs)
      throws IllegalAccessException, InstantiationException, ClassNotFoundException {
    this.accessLogs = accessLogs;
  }

  private Connection getConnection()
      throws ClassNotFoundException, IllegalAccessException, InstantiationException {
    Class.forName("com.mysql.jdbc.Driver").newInstance();
    Connection conn = null;
    try {
      conn =
          DriverManager.getConnection("jdbc:mysql://localhost:3306/db?user=devdb&password=devdb");
      return conn;

    } catch (SQLException ex) {
      // handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    }
    return null;
  }

  @Override
  public void run() {

    try (PreparedStatement preparedStatement = connection.prepareStatement(insertQuery); ) {
      System.out.println("Running inserting query at " + Thread.currentThread().getName() + "...");
      connection.setAutoCommit(false);
      int batchSize = 1000;
      int count = 0;
      for (AccessLog log : accessLogs) {
        try {
          preparedStatement.setTimestamp(1, Timestamp.valueOf(log.getDate()));
          preparedStatement.setString(2, log.getIp());
          preparedStatement.setString(3, log.getRequest());
          preparedStatement.setString(4, log.getStatus());
          preparedStatement.setString(5, log.getUserAgent());
          preparedStatement.setString(6, log.getComment());
          preparedStatement.addBatch();
          if (++count % batchSize == 0) {
            preparedStatement.executeBatch();
          }
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
      preparedStatement.executeBatch();
      System.out.println("Execution finish thread " + Thread.currentThread().getName());
      connection.commit();
    } catch (SQLException e) {
      e.printStackTrace();
      try {
        connection.rollback();
      } catch (SQLException e1) {
        e1.printStackTrace();
      }
    }
  }
}
